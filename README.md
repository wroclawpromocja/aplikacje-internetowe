![alt tag](http://www.tapetus.pl/obrazki/n/88468_grafika-tygrys-3d.jpg)

APLIKACJE INTERNETOWE
===============

HTML, CSS, JAVASCRIPT, PHP
----------------

Zakresem przedmiot ten obejmuje HTML, CSS, JAVASCRIPT, PHP.
W trakcie zajęć nauczane będzie również używanie bibliotek i frameworków
służących do budowania szybko i sprawnie interaktywnych aplikacji sieciowych.

Poznane zagadnienia będą tyczyły się aplikacji po obu stronach:
client and server side.

Linki warte uwagi (do nauki)
----------------

* http://www.htmldog.com/ :: HTML tutorials. And stuff.
* http://www.cheetyr.com/css-selectors
* http://www.jqueryrain.com/?A8hWnw3I :: podziwiaj animację wykonaną z pomocą JavaScript
* [jQuery.com](https://jquery.com/) oraz dokumentacja do tego frameworka https://api.jquery.com/
* [JSFiddle.net](https://jsfiddle.net/) :: testuj swoje pomysły, dziel się z innymi swoim kodem na https://jsfiddle.net/

* https://www.codecademy.com/tracks/web

* [www.Sololearn.com](http://www.sololearn.com/) :: kursy online z testami i filmikami z HTML CSS PHP JS i inne. kursy w jezyku ANG.

* http://flexboxfroggy.com/ gra 

**Mirosław ZELENT**

* http://miroslawzelent.pl/ :: Blog
* [Skuteczna nauka programowania](https://www.youtube.com/playlist?list=PLOYHgt8dIdoxUJ8j9AkLLFmBMBqS9LUeh) :: YouTube, vlog
* [Kurs HTML, Tworzenie zawartości stron](https://www.youtube.com/watch?v=1M0YXFW31hg&list=PLOYHgt8dIdox9Qq3X9iAdSVekS_5Vcp5r) :: YouTube, vlog
* [Kurs CSS, kaskadowe arkusze stylów](https://www.youtube.com/watch?v=RJEKMbD_kEk&list=PLOYHgt8dIdow6b2Qm3aTJbKT2BPo5iybv) :: YouTube, vlog
* [Kurs JavaScript, programowanie frontendowe](https://www.youtube.com/watch?v=OcwON22ctYc&list=PLOYHgt8dIdoxTUYuHS9ZYNlcJq5R3jBsC) :: YouTube, vlog
* [Kurs PHP, programowanie backendowe](https://www.youtube.com/watch?v=WSeKPbVZBoo&list=PLOYHgt8dIdox81dbm1JWXQbm2geG1V2uh) :: YouTube, vlog
* [Kurs MySQL, bazy danych, język zapytań SQL](https://www.youtube.com/watch?v=99JAI24Zd24&list=PLOYHgt8dIdoymv-Wzvs8M-OsKFD31VTVZ) :: YouTube, vlog

Lektura
---------------

Na samym początku każda książka do nauki wydaje się być właściwa. Niestety, czasem autor trafi ze swoją wiedzą do Ciebie, ale nie do mnie, zatem nie będę sugerował konkretnych tytułów. Opinie o poszczególnych tytułach dostępne są w Internecie, na stronach owych księgarni. Mamy XXI wiek, nie ma potrzeby zawierzania mi na słowo że dany tytuł jest dobry - no i... nie przeczytałem wszystkich książek... tak więc:

**HELION**

* [PHP, aplikacje po stronie serwera, kod generujący](http://helion.pl/kategorie/webmasterstwo/php)
* [HTML, CSS, JavaScript - tworzenie stron WWW](http://helion.pl/kategorie/webmasterstwo/tworzenie-stron-www)
* [JavaScript, Frameworki oraz inne biblioteki i aplikacje oparte o JS (JavaScript)](http://helion.pl/kategorie/webmasterstwo/javascript)
* [CSS, jak kolorować i formatować, podstawy oraz techniki zaawansowane](http://helion.pl/kategorie/webmasterstwo/css)
* [HTML, XHTML - "wyduś ze znaczników co się tylko da" - formatowanie dokumentów w oparciu o znaczniki](http://helion.pl/kategorie/webmasterstwo/html-i-xhtml)

**PWN - Polskie Wydawnictwo Naukowe**

* [HTML5 i CSS3 / Definicja nowoczesności](http://ksiegarnia.pwn.pl/produkt/280977/html5-i-css3.html) :: książka
* [Programowanie, książki wydawnictwa PWN](http://ksiegarnia.pwn.pl/kategoria/125281,20411/programowaniejezyki-programowania.html?od=60) :: e-księgarnia, polecany dział

Informacje dodatkowe
---------------

**>> GIT** _| kurs od a-z, język polski | **PL**_

* https://git-scm.com/book/pl/v1/Pierwsze-kroki-Podstawy-Git